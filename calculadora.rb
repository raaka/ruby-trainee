require_relative 'operacoes_extras'
module Calculadora

  class Operacoes
    include OperacoesExtras
    
    def fibonacci(n)
      a = 0
      b = 1

      n.times do
          temp = a
          a = b
          b = temp + b
      end

      return a
    end

    def fatorial(n)
      if n < 0
        return nil
      end
      
      result = 1
      while n > 0
        result = result * n
        n -= 1
      end
      
      return result
    end

    def resolver_equacao(a, b, c)
      # Calcula e verifica se delta possui raíz real
      delta = b*b - 4*a*c
      if(delta < 0)
          return nil
      else
          # Calcula o valor de x1 e x2
          x1 = (-b + Math.sqrt(delta)) / (2*a)
          x2 = (-b - Math.sqrt(delta)) / (2*a)
      end

      return x1,x2
    end
    
  end

  class Menu
    def initialize
      puts "Escolha uma das opções"
      puts "1. Calcular fibonacci"
      puts "2. Calcular fatorial"
      puts "3. Calcular equação do segundo grau"
      puts "4. Sair"
      puts "Digite um número para escolher uma das opções acima"

      choose = gets.chomp().to_s

      case choose
      when "1"
        puts "Você escolheu Fibonacci."
        puts "Digite um número inteiro positivo"
        n = gets.chomp().to_i
        #Chamar fibonacci
        op = Operacoes.new()
        result = op.fibonacci(n)
        puts ("Resultado: "+result.to_s)
      when "2"
        puts "Você escolheu fatorial."
        puts "Digite um número"
        n = gets.chomp().to_i
        #Chamar fatorial
        op = Operacoes.new()
        result = op.fatorial(n)
        puts ("Resultado: "+result.to_s)
      when "3"
        puts "Você escolheu Equacao do segundo grau."
        puts "Digite o valor de a"
        a = gets.chomp().to_i
        
        puts "Digite o valor de b"
        b = gets.chomp().to_i

        puts "Digite o valor de c"
        c = gets.chomp().to_i
        #Chamar fatorial
        op = Operacoes.new()
        result = op.resolver_equacao(a,b,c)
        puts ("Resultado: x1="+result[0].to_s+", x2="+result[1].to_s)
      when "4"
        exit
      else
        puts "Opção inválida"
      end
    end
  end
end
